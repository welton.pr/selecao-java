package com.indracompany.selecaojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan("com.indracompany.selecaojava.controller")
@EntityScan("com.indracompany.selecaojava")
@Configuration
@EnableSwagger2
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
    public Docket apiProjet() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
        		.select()
        	    .apis(RequestHandlerSelectors.basePackage("com.indracompany.selecaojava"))
                .paths(PathSelectors.regex("/api.*"))
                .build()
                .apiInfo(apiInfo());
        return docket;
    }
	
	private ApiInfo apiInfo() {
		 return new ApiInfo(
			"API seleção Indra Company", 
			"Projeto de apis para seleção java Indra Company.", 
			"1.0", 
			"Terms of service", 
			new Contact("Welton Silva", null, "welton_pr@yahoo.com.br"), 
			null, null);
	}
}


