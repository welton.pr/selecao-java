
package com.indracompany.selecaojava.controller.HistoricoPreco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.indracompany.selecaojava.model.Bandeira;
import com.indracompany.selecaojava.model.HistoricoPreco;
import com.indracompany.selecaojava.model.Municipio;
import com.indracompany.selecaojava.model.Produto;
import com.indracompany.selecaojava.model.Revenda;
import com.indracompany.selecaojava.model.enums.EstadoEnum;
import com.indracompany.selecaojava.model.enums.UnidadeMedidaEnum;
import com.indracompany.selecaojava.model.exceptions.ArquivoFormatoIncorretoException;
import com.indracompany.selecaojava.model.exceptions.ArquivoNaoEncontradoException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDateException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDoubleException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaLongException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorInformadoException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorNaoInformadoException;
import com.indracompany.selecaojava.model.exceptions.NaoEncontradoException;
import com.indracompany.selecaojava.utis.Conversor;
import com.indracompany.selecaojava.utis.TratamentosDeDados;

@Service
public class HistoricoPrecoService {	
	public static final boolean DEBUG = false;
	public static final int IDENTIFCADOR_ZERADO = 0;
	private static final String TIPO_ARQUIVO_CSV = "text/csv";
	@Autowired
	HistoricoPrecoRepository historicoPrecoRepository;
	@Autowired
	MunicipioRepository municipioRepository;
	@Autowired
	ProdutoRepository produtoRepository;
	@Autowired
	BandeiraRepository bandeiraRepository;
	@Autowired
	RevendaRepository revendaRepository;
	
	public HistoricoPreco inserir(HistoricoPreco historicoPreco) throws IdentificadorInformadoException {
		if(historicoPreco.getCodigo() == IDENTIFCADOR_ZERADO)
			return historicoPrecoRepository.save(historicoPreco);
		throw new IdentificadorInformadoException("Não deve enviar um histórico já cadastrado.");
	}

	public HistoricoPreco atualizar(HistoricoPreco historicoPreco) throws IdentificadorNaoInformadoException, NaoEncontradoException{
		if(historicoPreco.getCodigo() > IDENTIFCADOR_ZERADO) 
			if(historicoPrecoRepository.findById(historicoPreco.getCodigo()).isPresent()) 
				return historicoPrecoRepository.save(historicoPreco);
			else
				throw new NaoEncontradoException("Histórico de preço não encontrado");
		else
			throw new IdentificadorNaoInformadoException("Não informado o código de histórico de preço");	
	}
	
	public void deletarPorCodigo(Long codigo) throws NaoEncontradoException, IdentificadorNaoInformadoException {
		
		if(codigo > IDENTIFCADOR_ZERADO) 
			if(historicoPrecoRepository.findById(codigo).isPresent()) 
				historicoPrecoRepository.deleteById(codigo);
			else
				throw new NaoEncontradoException("Histórico de preço não encontrado");
		
		else
			throw new IdentificadorNaoInformadoException("Não informado o código do histórico de preço");	
			
	}

	public Optional<HistoricoPreco> selecionarPorId(Long codigo) {
		return historicoPrecoRepository.findById(codigo);
	}

	public List<HistoricoPreco> listarTodos() {
		return historicoPrecoRepository.findAll();
	}
	
	public void importCSV(MultipartFile file) throws ConversaoStringParaDateException, ConversaoStringParaDoubleException, ConversaoStringParaLongException, ArquivoNaoEncontradoException, ArquivoFormatoIncorretoException {
		 if (file.isEmpty()) 
	         throw new ArquivoNaoEncontradoException("Arquivo não encontrado na requisição");
		 if(!TIPO_ARQUIVO_CSV.equals(file.getContentType())) 
			 throw new ArquivoFormatoIncorretoException("Arquivo não esta no formato "+TIPO_ARQUIVO_CSV);
		Conversor conversor = new Conversor();
		List<HistoricoPreco> listaHistoricoPreco = new ArrayList<HistoricoPreco>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream(), "iso-8859-1"))) {
			final int  posicaolinhaCabecalho = 0;
			String delimiterRegex = "\\s\\s";
			String line;
			int posicaoLinhaAtual = 0;
			while ((line = br.readLine()) != null) {
				if(posicaoLinhaAtual == posicaolinhaCabecalho) {
					posicaoLinhaAtual = 1;
					continue;
				}
				String[] valores = line.split(delimiterRegex);
			
				TratamentosDeDados tratarDados = new TratamentosDeDados();
				valores = tratarDados.nomeRevenda2Espacos(valores, posicaoLinhaAtual);
				valores = tratarDados.removerEspacosCampoEstado(valores, posicaoLinhaAtual);
				valores = tratarDados.nomePostoComEspacos(valores, posicaoLinhaAtual);
				
				HistoricoPreco historicoPreco = new HistoricoPreco(0, 
					valores[0], 
					(new Municipio(0, valores[2], EstadoEnum.valueOf(valores[1]))), 
					(new Revenda(conversor.convertStringToLong(valores[4], posicaoLinhaAtual), valores[3])), 
					(new Produto(0, valores[5])), 
					conversor.convertStringToDate(valores[6], "dd/MM/yyyy", posicaoLinhaAtual), 
					conversor.convertStringToDouble(valores[7], posicaoLinhaAtual), 
					conversor.convertStringToDouble(valores[8], posicaoLinhaAtual), 
					UnidadeMedidaEnum.valueOf(valores[9].replace("R$ / ", "")), 
					(new Bandeira(0,valores[10])));
				listaHistoricoPreco.add(historicoPreco);
				posicaoLinhaAtual++;
			}
		
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		Map<String, Municipio> listaMunicipios = ((listaHistoricoPreco.stream()
			.map(historicoPreco -> {return historicoPreco.getMunicipio();})
			.distinct().collect(Collectors.toList()))
		.stream()
		.map((municipio) -> {
			return (municipioRepository.existsByNome(municipio.getNome())
				?municipioRepository.findOneByNome(municipio.getNome())
				:municipioRepository.saveAndFlush(municipio));
		}).distinct().collect(Collectors.toMap(Municipio::getNome, Function.identity())));		
		
				
		Map<String, Bandeira> listaBandeiras = ((listaHistoricoPreco.stream()
			.map(historicoPreco -> historicoPreco.getBandeira())
			.distinct().collect(Collectors.toList()))
		.stream().map((bandeira) -> {
			return (bandeiraRepository.existsByDescricao(bandeira.getDescricao())
				?bandeiraRepository.findByDescricao(bandeira.getDescricao())
				:bandeiraRepository.saveAndFlush(bandeira));
		}).collect(Collectors.toMap(Bandeira::getDescricao, Function.identity())));		

		
		Map<String, Produto> listaProdutos = ((listaHistoricoPreco.stream()
			.map(historicoPreco -> historicoPreco.getProduto())
			.distinct().collect(Collectors.toList()))
		.stream().map((produto) -> {
			return (produtoRepository.existsByNome(produto.getNome())
				?produtoRepository.findOneByNome(produto.getNome())
				:produtoRepository.saveAndFlush(produto));
		}).collect(Collectors.toMap(Produto::getNome, Function.identity())));		

		
		Map<Long, Revenda> listaRevendasAux = (listaHistoricoPreco.stream()
			.map(historicoPreco -> historicoPreco.getRevenda())
			.distinct().collect(Collectors.toMap(Revenda::getCodigoInstalacao, Function.identity())));
		Map<Long, Revenda> listaRevendas = revendaRepository.saveAll(listaRevendasAux.values()).stream().collect(Collectors.toMap(Revenda::getCodigoInstalacao, Function.identity()));		

		
		listaHistoricoPreco = listaHistoricoPreco.stream()
			.filter(historicoPreco -> {
				historicoPreco.setRevenda(listaRevendas.get(historicoPreco.getRevenda().getCodigoInstalacao()));
				historicoPreco.setMunicipio(listaMunicipios.get(historicoPreco.getMunicipio().getNome()));
				historicoPreco.setBandeira(listaBandeiras.get(historicoPreco.getBandeira().getDescricao()));
				historicoPreco.setProduto(listaProdutos.get(historicoPreco.getProduto().getNome()));
				return true;
			})
			.collect(Collectors.toList());
			
			historicoPrecoRepository.saveAll(listaHistoricoPreco);
	}
}
