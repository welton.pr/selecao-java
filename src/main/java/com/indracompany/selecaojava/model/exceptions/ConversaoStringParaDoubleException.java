package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class ConversaoStringParaDoubleException extends AplicacaoException{

	private static final long serialVersionUID = 1963372190825441186L;
	
	public ConversaoStringParaDoubleException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}
	
}
