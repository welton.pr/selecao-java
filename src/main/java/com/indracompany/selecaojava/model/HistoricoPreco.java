package com.indracompany.selecaojava.model;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.indracompany.selecaojava.model.enums.UnidadeMedidaEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoricoPreco {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long codigo; 
	private String RegiaoSigla;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Municipio Municipio;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Revenda revenda;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Produto produto;
	@Temporal(TemporalType.DATE)
	private Date DataColeta;
	private double valorCompra;
	private double valorVenda;
	private UnidadeMedidaEnum UnidadeMedida;
	@OneToOne(cascade = {CascadeType.ALL})
	private Bandeira bandeira;
	
}
