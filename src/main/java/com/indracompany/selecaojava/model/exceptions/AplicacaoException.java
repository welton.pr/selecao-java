package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class AplicacaoException extends Exception{

	private static final long serialVersionUID = 2788746109062376686L;
	
	public HttpStatus httpStatus;
	
	public AplicacaoException(String message, HttpStatus httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}
	
}
