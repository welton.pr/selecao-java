package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class ArquivoFormatoIncorretoException extends AplicacaoException{

	private static final long serialVersionUID = -6831652631755424515L;

	public ArquivoFormatoIncorretoException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

	
	
}
