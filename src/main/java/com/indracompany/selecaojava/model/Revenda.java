package com.indracompany.selecaojava.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Revenda {
	@Id
	@Column(name = "codigo_instalacao")
	private long codigoInstalacao;
	private String nome;
//	@OneToMany(cascade = {CascadeType.MERGE},fetch= FetchType.EAGER)
//	private List<HistoricoPreco> listaHistoricoPrecos;
}
