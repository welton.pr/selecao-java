package com.indracompany.selecaojava;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTests.class)
@TestPropertySource(locations="classpath:tests.properties")
public class ApplicationTests {

	@Test
	void contextLoads() {
	}

}
