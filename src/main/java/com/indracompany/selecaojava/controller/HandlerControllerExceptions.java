package com.indracompany.selecaojava.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.indracompany.selecaojava.model.Erro;
import com.indracompany.selecaojava.model.exceptions.AplicacaoException;

public class HandlerControllerExceptions {
		
	@ExceptionHandler({ AplicacaoException.class})
    public ResponseEntity<Object> handleNaoEncontradoException(AplicacaoException aplicacaoException, WebRequest request) {   
		List<String> details = new ArrayList<String>();
        details.add(aplicacaoException.getLocalizedMessage());
        Erro erro = new Erro(aplicacaoException.getMessage());
        return new ResponseEntity<Object>(erro, aplicacaoException.httpStatus);
    }
}
