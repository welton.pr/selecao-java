package com.indracompany.selecaojava.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Erro {
	
	private String mensagem;
	
}
