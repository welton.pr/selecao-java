package com.indracompany.selecaojava.utis;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.datetime.DateFormatter;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDateException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDoubleException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaLongException;

public class Conversor {

	
	
	public double convertStringToDouble(String args, int linha) throws ConversaoStringParaDoubleException {
		if(args == null || "".equals(args))
			return 0.0;
	
		args = args.replace(".", "");
		args = args.replace(",", ".");
		try {
			
			return Double.parseDouble(args);
		}
		catch (NullPointerException|NumberFormatException e) {
			throw new ConversaoStringParaDoubleException("Erro ao converter para monetário o valor "+args+" na linha "+linha);
		}
	}
	
	public Date convertStringToDate(String args, String pattern, int linha) throws ConversaoStringParaDateException, ConversaoStringParaDoubleException {
		try {
			return new DateFormatter(pattern).parse(args, Locale.getDefault());
		} catch (ParseException e) {
			throw new ConversaoStringParaDoubleException("Erro ao converter para data o valor "+args+" na linha "+linha);
		}
	}
	
	public Long convertStringToLong(String args, int linha) throws ConversaoStringParaLongException {
		if(args == null || "".equals(args))
			return (long) 0;
		
		try {
			return Long.parseLong(args);
		} catch (NumberFormatException|NullPointerException e) {
			throw new ConversaoStringParaLongException(args);
		}
	}
	
}
