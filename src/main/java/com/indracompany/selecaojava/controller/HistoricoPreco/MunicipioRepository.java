package com.indracompany.selecaojava.controller.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.Municipio;

@org.springframework.stereotype.Repository
public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

	public Municipio findOneByNome(String nome);
	
	public boolean existsByNome(String nome);
	
}
