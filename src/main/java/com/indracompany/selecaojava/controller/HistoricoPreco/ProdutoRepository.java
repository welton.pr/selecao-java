package com.indracompany.selecaojava.controller.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.Produto;

@org.springframework.stereotype.Repository
public interface ProdutoRepository extends JpaRepository<Produto, Long> {

	public Produto findOneByNome(String nome);
	
	public boolean existsByNome(String nome);
	
}
