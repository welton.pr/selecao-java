package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class NaoEncontradoException extends AplicacaoException{

	private static final long serialVersionUID = 1L;

	public NaoEncontradoException(String message) {
		super(message, HttpStatus.NO_CONTENT);
	}
}
