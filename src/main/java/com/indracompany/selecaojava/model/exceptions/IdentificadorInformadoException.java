package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class IdentificadorInformadoException extends AplicacaoException{

	private static final long serialVersionUID = 1L;
	
	public IdentificadorInformadoException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}
	
}
