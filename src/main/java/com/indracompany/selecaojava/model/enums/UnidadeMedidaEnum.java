package com.indracompany.selecaojava.model.enums;

import lombok.Getter;

public enum UnidadeMedidaEnum {
	litro("R$ / litro");
	@Getter private final String valor;

	private UnidadeMedidaEnum(String valor) {
		this.valor = valor;
	} 
}
