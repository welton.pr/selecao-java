package com.indracompany.selecaojava.controller.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.HistoricoPreco;

@org.springframework.stereotype.Repository
public interface HistoricoPrecoRepository extends JpaRepository<HistoricoPreco, Long> {
	
}
