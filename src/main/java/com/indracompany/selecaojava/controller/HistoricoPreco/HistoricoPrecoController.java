package com.indracompany.selecaojava.controller.HistoricoPreco;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.indracompany.selecaojava.controller.HandlerControllerExceptions;
import com.indracompany.selecaojava.model.HistoricoPreco;
import com.indracompany.selecaojava.model.exceptions.ArquivoFormatoIncorretoException;
import com.indracompany.selecaojava.model.exceptions.ArquivoNaoEncontradoException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDateException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaDoubleException;
import com.indracompany.selecaojava.model.exceptions.ConversaoStringParaLongException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorInformadoException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorNaoInformadoException;
import com.indracompany.selecaojava.model.exceptions.NaoEncontradoException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("api/v1/historico-preco")
@Api(tags = "histórico de preços")
public class HistoricoPrecoController extends HandlerControllerExceptions{
	@Autowired
	private HistoricoPrecoService historicoPrecoService;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Criar histórico de preço", notes = "Serviço que adiciona novo histórico de preço")
	public HistoricoPreco inserir(@RequestBody HistoricoPreco usuario) throws IdentificadorInformadoException {
		return historicoPrecoService.inserir(usuario);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Atualiza histórico de preço", notes = "Serviço que atualiza histórico de preço")
	public HistoricoPreco atualizar(@RequestBody HistoricoPreco usuario) throws IdentificadorNaoInformadoException, NaoEncontradoException{
		return historicoPrecoService.atualizar(usuario);
	}
	
	@DeleteMapping(value = "/{codigo}")
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Remover histórico de preço por código", notes = "Serviço que remove o histórico de preço por código")
	public void deletar(@PathVariable("codigo") long codigo) throws IdentificadorNaoInformadoException, NaoEncontradoException {
		historicoPrecoService.deletarPorCodigo(codigo);
	}
	
	@GetMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Procurar histórico de preço", notes = "Serviço que procura um histórico de preço por código")
	public Optional<HistoricoPreco> usuario(@PathVariable("codigo") long codigo) {
		return historicoPrecoService.selecionarPorId(codigo);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Listar histórico de preço", notes = "Serviço que lista todo histórico de preços")
	public List<HistoricoPreco> usuarios() {
		return historicoPrecoService.listarTodos();
	}
	
	@PostMapping(value ="importCsv")
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Importar histórico de preços", notes = "Serviço que recer arquivo csv e importa para o histórico")
	public String importCSVHistoricoPrecos(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws ConversaoStringParaDoubleException, ConversaoStringParaDateException, ConversaoStringParaLongException, ArquivoNaoEncontradoException, ArquivoFormatoIncorretoException  {
			historicoPrecoService.importCSV(file);
			return "CSV enviado com sucesso";
	}
}
