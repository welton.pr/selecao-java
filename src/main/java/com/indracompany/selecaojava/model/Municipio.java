package com.indracompany.selecaojava.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.indracompany.selecaojava.model.enums.EstadoEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Municipio {
	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long codigo;
	private String nome;
	private EstadoEnum estado;
	
}
