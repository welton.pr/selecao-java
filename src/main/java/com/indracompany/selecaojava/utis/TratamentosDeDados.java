package com.indracompany.selecaojava.utis;

public class TratamentosDeDados {

	//Bug no csv 1
	public String[] nomeRevenda2Espacos(String[] valores, int linha){
		
		//String temPedacosNomeDeCampoAnterior = "[A-Za-z\\s\\.\\&\\-ÁÉÍÓÚÊÂÎÛÂÔÇ]+";
		String temSomenteNumeros = "^\\d{1,100}(\\.\\d{1,2})?$";
		int ValorParaVoltarPosicao = 1;
		boolean TemQueArrumar = true;
		while(TemQueArrumar) {
			if(!valores[4].matches(temSomenteNumeros)) {
				valores[3] = valores[3]+" "+valores[4];
				for(int i = 4; i < (valores.length-ValorParaVoltarPosicao); i++) {
					valores[i] = valores[i+ValorParaVoltarPosicao];
				}
				TemQueArrumar = true;
			}else {
				TemQueArrumar = false;
			}
		}
		return valores;
	}
	
	//Bug no csv 2
	public String[] removerEspacosCampoEstado(String[] valores, int linha){
		
		String temNomeComEspacos = "[A-Za-z\\s]+";
		
		if(valores[1].matches(temNomeComEspacos)) {
			valores[1] = valores[1].trim();
		}
		return valores;
	}
	
	//Bug no csv 3
	public String[] nomePostoComEspacos(String[] valores, int linha){
		
		int ValorParaVoltarPosicao = 1;
		
		boolean TemQueArrumar = true;
		while(TemQueArrumar) {
			if("".equals(valores[3])) {
				for(int i = 3; i < (valores.length-ValorParaVoltarPosicao); i++) {
					valores[i] = valores[i+ValorParaVoltarPosicao];
				}
				TemQueArrumar = true;
			}else {
				TemQueArrumar = false;
			}
		}
		return valores;
	}
	
}
