package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class ArquivoNaoEncontradoException extends AplicacaoException{

	public ArquivoNaoEncontradoException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}

	private static final long serialVersionUID = 8471420067602398956L;
	
}
