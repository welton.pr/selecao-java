package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class ConversaoStringParaDateException extends AplicacaoException{

	private static final long serialVersionUID = 3298954632860767754L;

	
	public ConversaoStringParaDateException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}
}
