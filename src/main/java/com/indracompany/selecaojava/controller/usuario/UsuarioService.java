package com.indracompany.selecaojava.controller.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.indracompany.selecaojava.model.Usuario;
import com.indracompany.selecaojava.model.exceptions.IdentificadorInformadoException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorNaoInformadoException;
import com.indracompany.selecaojava.model.exceptions.NaoEncontradoException;

@Service
public class UsuarioService {	
	
	@Autowired
	UsuarioRepository usuarioRepository;
	public static final int IDENTIFCADOR_ZERADO = 0;
	
	
	public Usuario inserir(Usuario usuario) throws IdentificadorInformadoException {
		
		if(usuario.getCodigo() == IDENTIFCADOR_ZERADO)
			return usuarioRepository.save(usuario);
		
		throw new IdentificadorInformadoException("Código não deve ser informado do usuário");

	}

	public Usuario atualizar(Usuario usuario) throws IdentificadorNaoInformadoException, NaoEncontradoException{

		if(usuario.getCodigo() > IDENTIFCADOR_ZERADO) 
			if(usuarioRepository.findById(usuario.getCodigo()).isPresent()) 
				return usuarioRepository.save(usuario);
			else
				throw new NaoEncontradoException("Não encontrado usuário ");
		else
			throw new IdentificadorNaoInformadoException("Código de usuário não foi informado");	

	}
	
	public void deletarPorCodigo(Long codigo) throws NaoEncontradoException, IdentificadorNaoInformadoException {
		
		if(codigo > IDENTIFCADOR_ZERADO) 
			if(usuarioRepository.findById(codigo).isPresent()) 
				usuarioRepository.deleteById(codigo);
			else
				throw new NaoEncontradoException("Código de usuário não encontrado");
		else
			throw new IdentificadorNaoInformadoException("Código de usuario não informado");	
			
	}

	public Optional<Usuario> selecionarPorCodigo(Long codigo) {
		return usuarioRepository.findById(codigo);
	}

	public List<Usuario> listarTodos() {
		return usuarioRepository.findAll();
	}
}
