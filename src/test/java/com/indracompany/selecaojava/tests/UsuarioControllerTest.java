package com.indracompany.selecaojava.tests;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.indracompany.selecaojava.ApplicationTests;
import com.indracompany.selecaojava.controller.usuario.UsuarioController;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UsuarioController.class)
public class UsuarioControllerTest extends ApplicationTests{
	
	@Autowired private MockMvc mockMvc;
	
	@MockBean private UsuarioController usuarioController;
	
	
	@Before
	public void setUp() {
	}

	@Test
	public void testGetUsuario() throws Exception {
		Mockito.when(usuarioController.retrieveCourse(Mockito.anyObject()), Mockito.anyObject()).thenReturn(mockUsuario"));
		
		this.mockMvc.perform(MockMvcRequestBuilders.get("api/v1/usuarios")).andExpect(MockMvcResultMatchers.status().isBadGateway());
		this.mockMvc = MockMvcBuilders.standaloneSetup(usuarioController).build();

	}
}
