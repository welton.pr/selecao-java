package com.indracompany.selecaojava.controller.usuario;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.indracompany.selecaojava.controller.HandlerControllerExceptions;
import com.indracompany.selecaojava.model.Usuario;

import com.indracompany.selecaojava.model.exceptions.IdentificadorInformadoException;
import com.indracompany.selecaojava.model.exceptions.IdentificadorNaoInformadoException;
import com.indracompany.selecaojava.model.exceptions.NaoEncontradoException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("api/v1/usuarios")
@Api(tags = "usuario")
public class UsuarioController extends HandlerControllerExceptions {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Criar usuário", notes = "Serviço recebe dados e cria usuário", response = Usuario[].class)
	public Usuario inserir(@RequestBody Usuario usuario) throws IdentificadorInformadoException {
		return usuarioService.inserir(usuario);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Atualiza usuário", notes = "Serviço recebe dados e atualiza usuário")
	public Usuario atualizar(@RequestBody Usuario usuario) throws IdentificadorNaoInformadoException, NaoEncontradoException{
		return usuarioService.atualizar(usuario);
	}
	
	@DeleteMapping(value = "/{codigo}")
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Remover usuário por código", notes = "Serviço que remove o usuário por código")
	public void deletar(@PathVariable("codigo") long codigo) throws IdentificadorNaoInformadoException, NaoEncontradoException {
		usuarioService.deletarPorCodigo(codigo);
	}
	
	@GetMapping(value = "/{codigo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Procurar um usuário por código", notes = "Serviço que procura um usuário por código")
	public Optional<Usuario> selecionarPorCodigo(@PathVariable("codigo") long codigo) {
		return usuarioService.selecionarPorCodigo(codigo);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation(value = "Listar usuários", notes = "Serviço que lista todos usuários")
	public List<Usuario> listarTodos() {
		return usuarioService.listarTodos();
	}
}
