package com.indracompany.selecaojava.controller.usuario;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.Usuario;

@org.springframework.stereotype.Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
}
