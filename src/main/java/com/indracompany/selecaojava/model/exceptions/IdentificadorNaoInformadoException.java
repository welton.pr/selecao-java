package com.indracompany.selecaojava.model.exceptions;

import org.springframework.http.HttpStatus;

public class IdentificadorNaoInformadoException extends AplicacaoException {

	private static final long serialVersionUID = 1L;

	public IdentificadorNaoInformadoException(String message) {
		super(message, HttpStatus.BAD_REQUEST);
	}
}
