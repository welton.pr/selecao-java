package com.indracompany.selecaojava.controller.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.Revenda;

@org.springframework.stereotype.Repository
public interface RevendaRepository extends JpaRepository<Revenda, Long> {}
