package com.indracompany.selecaojava.model.exceptions;

public class ConversaoStringParaLongException extends Exception{

	private static final long serialVersionUID = 711806642232732589L;

	public ConversaoStringParaLongException(String message) {
		super(message);
	}
}
