package com.indracompany.selecaojava.controller.HistoricoPreco;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indracompany.selecaojava.model.Bandeira;

@org.springframework.stereotype.Repository
public interface BandeiraRepository extends JpaRepository<Bandeira, Long> {

	public Bandeira findByDescricao(String descricao);

	public boolean existsByDescricao(String descricao);


}
